!! Default Modtool File descriptions. You can overwrite them in your own module by putting a "filedesc" file at module root.
!! nb : all non alpha chars must be replaced by _

Dataindex=<small>(auto generated) - bestanden index</small>
NEWS=Noteer hier de veranderingen aan uw module
README=Leesmij eerst bestand
about_phtml='About' file.
content_html=<small>(auto generated)</small>
endhook_phtml=exercices footer
help_phtml=User help
main_phtml=Main file
intro_phtml=Introduction and exercices customization
introhook_phtml=Automatically list all available exercices

src_cpp_qcm_cpp=This CPP file generates all OEF exercices in this module
src_cpp_confparm_inc=Custom parameters
src_cpp_author_inc=Author informations

src_ordered_oef=<small>(CPP Generated) - Ordered questions.</small>
src_serial_oef=<small>(CPP Generated) - Serial questions.</small>
src_shuffle_oef=<small>(CPP Generated) - Shuffle questions.</small>

var_proc=General variables processing
var_init=Variables initialization
var_def=Variables definitions

filedesc=Explain each file in this module
