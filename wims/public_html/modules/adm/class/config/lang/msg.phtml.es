
!set wims_module_log=error: $error $wims_class

<b>$wims_name_Error</b>.

!if not_secure iswordof $error
 !if $code!=$empty
  C�digo secreto incorrecto.
 !else
  A este m�dulo s�lo se puede acceder desde una estaci�n de trabajo segura.
 !endif
 Acceso rechazado.
 !exit
!endif

!if checkhost iswordof $error
 Su conexi�n actual no forma parte de las definidas como estaciones de
 trabajo seguras. Se trata probablemente de un error que puede hacerle perder
 el control de la clase. En consecuencia no se ha modificado la relaci�n de
 estaciones de trabajo seguras.
 !exit
!endif

!if bad_file iswordof $error
 Lo sentimos pero no reconocemos <em>$wims_deposit</em>
 como copia de seguridad de una clase. Es posible que haya enviado un fichero
 incorrecto o que haya sido da�ado.
 !exit
!endif

!if empty_file iswordof $error
 No encontramos recursos de clase en su copia de seguridad.
 <span class="tt wims_fname">$wims_deposit</span>.
 !exit
!endif

!if no_upload iswordof $error
 Los datos enviados han expirado. Acci�n fallida, por favor reint�ntela.
 !exit
!endif

!if remote_sharing iswordof $error
 Una clase vecina comparte sus recursos. Antes de que deje de compartirlos,
 usted ni puede negarse a que siga comparti�ndolos ni puede eliminarla de
 su lista de vecinas.
 !exit
!endif

!if share_dep iswordof $error
 Al compartir recursos es necesario respetar las dependencias: debe compartir
 los ejercicios antes de compartir sus hojas de trabajo y debe compartir
 las hojas de trabajo antes de compartir las hojas de examen.
 !exit
!endif

!if stopshare_dep iswordof $error
 Le partage de ressources doit respecter les d�pendances&nbsp;: vous devez d'abord arr&ecirc;ter le partage 
 !if sheet iswordof $error
  des examens et du livret de comp�tences avant celui des feuilles de travail.
 !else
  !if exo iswordof $error
   des feuilles de travail avant celui des exercices.
  !else
   !if user iswordof $error
    des photos individuelles avant celui des comptes de participants.
   !endif
  !endif
 !endif
 !exit
!endif

!if not_neighbor iswordof $error
 Ha intentado realizar una operaci�n con una clase que no es su vecina
 efectiva.
 <br />
 Recuerde que una clase es su vecina efectiva �nicamente si se han declarado
 las vecindades mutuas A LA VEZ por usted y por la otra clase.
 !exit
!endif

!if localmirror iswordof $error
 No tiene sentido hacer una r�plica de una clase en el mismo servidor.
 !exit
!endif

!if toobigfile iswordof $error
 El tama�o de su fichero es demasiado importante: el l�mite es de 12K.
 !exit
!endif

!if file_too_large iswordof $error
  <p>It was not possible to archive this class because of its size. Archives are limited to <strong>$max_arch_size KB</strong>.</p>
  <p>We invite you to make a selective backup by selecting only certain data.</p>
  !if $format!=zip
    <p>nb: you can still download the generated file, but it is certainly truncated.</p>
  !endif
  !exit
!endif

!msg $error
