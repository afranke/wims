#! /bin/sh

log_dir=$w_wims_home/log/classes/$w_wims_class;
cat $log_dir/score/$w_getraw $log_dir/noscore/$w_getraw 2>/dev/null | sort >$session_dir/user.getraw;

if [ ! -s "$session_dir/user.getraw" ]; then
 echo "No work has been done by this participant up to now."
 exit
fi
if [ "$w_test" = "no" ]; then
 echo "    Date.Hour 	session	sheet exo Requete 	Statut"
 echo "------------------------------------------------------------"
 cat $session_dir/user.getraw | cut -d"	" -f1,4 ;
else
 echo "    Date.Hour 	session	sheet exo Requete 	IP 	Statut"
 echo "------------------------------------------------------------"
 if [ "$w_wims_user" = "supervisor" ]; then
  cat $session_dir/user.getraw | cut -d"	" -f1,2,3,4 ;
 else
  cat $session_dir/user.getraw | cut -d"	" -f1,2,4 ;
 fi;
fi;

## 2:IP, 3:seed, 4:status
