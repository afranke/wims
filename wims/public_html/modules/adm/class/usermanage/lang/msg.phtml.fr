!set wims_module_log=error: $error

<span class="wims_warning">$wims_name_Error</span>.

!if no_reccourse=$error
  Le transfert de ces activit�s n'est pas possible.
  !exit
!endif

!if not_supervisor=$error
 D�sol�, mais l'op�ration de pr�paration / modification d'une feuille de
 travail est r�serv�e aux enseignants enregistr�s d'une classe.
 !exit
!endif

!if bad_classpass=$error
 D�sol� mais vous n'avez pas fourni le bon mot de passe d'inscription de
 participant � la classe. Vous n'avez pas le droit de changer les informations
 qui suivent sans ce mot de passe.
 !exit
!endif

!if bad_user=$error
 Erreur d'appel : le participant <span class="tt wims_login">$checkuser</span> n'existe pas.
 !exit
!endif

!if badimgformat=$error
 Le fichier que vous avez envoy� n'est pas une image.
 !exit
!endif

!if filetoobig=$error
 Le fichier que vous avez envoy� d�passe la capacit� maximale autoris� apr�s conversion de l'image. Veuillez envoyer une image moins volumineuse.
 !exit
!endif

!if quota_file=$error
 La capacit� maximale de stockage de la classe est atteinte. Il n'est pas possible de sauvegarder votre image.
 !exit
!endif

!msg $error

