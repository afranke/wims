!!abstract,linked gloses,internal links,content,dynamic examples,...
!set gl_author=
!set gl_title=
!set gl_keywords=real_function

:
:
:
:
<div class="wims_defn">
<h4>D�finition</h4>Soit \(f\) une fonction num�rique d�finie sur un intervalle \(I\).<br/>
Soit \(a\) un r�el de \(I\) tel que \(f\) soit d�rivable en \(a\), de nombre d�riv�
<math xmlns='http://www.w3.org/1998/Math/MathML'>
 <mrow>
  <mi fontstyle='normal'>L</mi>
  <mo>=</mo>
  <mrow>
   <msup>
    <mi>f</mi>
    <mo>&#8242;</mo>
   </msup>
   <mo>(</mo>
   <mi>a</mi>
   <mo>)</mo>
  </mrow>
 </mrow>
</math>.<br/>
On note \(C\) la courbe repr�sentative de \(f\) dans un rep�re orthogonal du plan.<br/>
La <strong>tangente</strong> � \(C\) au point \(A\)
<math xmlns='http://www.w3.org/1998/Math/MathML'>
 <mrow>
  <mo>(</mo>
  <mrow>
   <mi>a</mi>
   <mo>;</mo>
   <mrow>
    <mi>f</mi>
    <mo>&#8289;</mo>
    <mo>(</mo>
    <mi>a</mi>
    <mo>)</mo>
   </mrow>
  </mrow>
  <mo>)</mo>
 </mrow>
</math> est la droite passant par \(A\) de coefficient directeur \(L\).
</div>
<div class="wims_thm">
<h4>Th�or�me</h4>Soit \(f\) une fonction num�rique d�rivable en un r�el \(a\) de nombre d�riv� <math xmlns='http://www.w3.org/1998/Math/MathML'>
 <mrow>
  <mi fontstyle='normal'>L</mi>
  <mo>=</mo>
  <mrow>
   <msup>
    <mi>f</mi>
    <mo>&#8242;</mo>
   </msup>
   <mo>(</mo>
   <mi>a</mi>
   <mo>)</mo>
  </mrow>
 </mrow>
</math>.<br/>Soit \(C\) sa courbe repr�sentative dans le plan muni d'un rep�re orthogonal.
<br/>
L'�quation r�duite de la tangente \(T\) � \(C\) au point d'abscisse \(a\) est
<math display='block' xmlns='http://www.w3.org/1998/Math/MathML'>
 <mrow>
  <mi>y</mi>
  <mo>=</mo>
  <mrow>
   <mrow>
    <mrow>
     <msup>
      <mi>f</mi>
      <mo>&#8242;</mo>
     </msup>
     <mo>(</mo>
     <mi>a</mi>
     <mo>)</mo>
    </mrow>
    <mo>&#215;</mo>
    <mrow>
     <mo>(</mo>
     <mrow>
      <mi>x</mi>
      <mo>-</mo>
      <mi>a</mi>
     </mrow>
     <mo>)</mo>
    </mrow>
   </mrow>
   <mo>+</mo>
   <mrow>
    <mi>f</mi>
    <mo>&#8289;</mo>
    <mo>(</mo>
    <mi>a</mi>
    <mo>)</mo>
   </mrow>
  </mrow>
 </mrow>
 <mi>.</mi>
</math>
</div>
