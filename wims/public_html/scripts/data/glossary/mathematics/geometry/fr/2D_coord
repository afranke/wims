!!abstract,linked gloses,internal links,content,dynamic examples,...
!set gl_author=Euler, Acad�mie de Versailles
!set gl_keywords=
!set gl_title=Coordonn�es d'un vecteur du plan
!!Pr�voir des illustrations
:
:
:
:
Le plan est suppos� muni d'un rep�re
  <math xmlns='http://www.w3.org/1998/Math/MathML'>
   <mrow>
    <mo>(</mo>
    <mrow>
     <mi fontstyle='normal'>O</mi>
     <mo>,</mo>
     <mover>
      <mi>i</mi>
      <mo>&#8594;</mo>
     </mover>
     <mo>,</mo>
     <mover>
      <mi>j</mi>
      <mo>&#8594;</mo>
     </mover>
    </mrow>
    <mo>)</mo>
   </mrow>
  </math>.
<div class="wims_thm"><h4>Th�or�me</h4>
Soit
  <math xmlns='http://www.w3.org/1998/Math/MathML'>
   <mover>
    <mi>v</mi>
     <mo>&#8594;</mo>
   </mover>
  </math>
 un vecteur du plan. <br/>
Il existe un unique couple
  \((x;y)\)
 de nombres r�els tel que :
   <math xmlns='http://www.w3.org/1998/Math/MathML'>
   <mrow>
    <mover>
    <mi>v</mi>
     <mo>&#8594;</mo>
   </mover>
    <mo>=</mo>
    <mrow>
     <mrow>
      <mi>x</mi>
      <mo>&#8290;</mo>
      <mover>
       <mi>i</mi>

        <mo>&#8594;</mo>

      </mover>
     </mrow>
     <mo>+</mo>
     <mrow>
      <mi>y</mi>
      <mo>&#8290;</mo>
      <mover>
       <mi>j</mi>

        <mo>&#8594;</mo>

      </mover>
     </mrow>
    </mrow>
   </mrow>
  </math>.
</div>
<div class="wims_defn">
<h4>D�finitions</h4>
Soit
  <math xmlns='http://www.w3.org/1998/Math/MathML'>
   <mover>
    <mi>v</mi>
     <mo>&#8594;</mo>
   </mover>
  </math>
 un vecteur du plan. <br/>
Les r�els uniques \(x\) et \(y\) tels que
  <math xmlns='http://www.w3.org/1998/Math/MathML'>
   <mrow>
    <mover>
    <mi>v</mi>
     <mo>&#8594;</mo>
   </mover>
    <mo>=</mo>
    <mrow>
     <mrow>
      <mi>x</mi>
      <mo>&#8290;</mo>
      <mover>
       <mi>i</mi>
        <mo>&#8594;</mo>
      </mover>
     </mrow>
     <mo>+</mo>
     <mrow>
      <mi>y</mi>
      <mo>&#8290;</mo>
      <mover>
       <mi>j</mi>
        <mo>&#8594;</mo>
      </mover>
     </mrow>
    </mrow>
   </mrow>
  </math>
sont les <strong>coordonn�es</strong> du vecteur
  <math xmlns='http://www.w3.org/1998/Math/MathML'>
   <mover>
    <mi>v</mi>
     <mo>&#8594;</mo>
   </mover>
  </math>
 dans le rep�re
  <math xmlns='http://www.w3.org/1998/Math/MathML'>
   <mrow>
    <mo>(</mo>
    <mrow>
     <mi fontstyle='normal'>O</mi>
     <mo>,</mo>
     <mover>
      <mi>i</mi>
      <mo>&#8594;</mo>
     </mover>
     <mo>,</mo>
     <mover>
      <mi>j</mi>
      <mo>&#8594;</mo>
     </mover>
    </mrow>
    <mo>)</mo>
   </mrow>
  </math>.<br/>
\(x\) est l'<strong>abscisse</strong>, \(y\) est l'<strong>ordonn�e</strong> du vecteur
  <math xmlns='http://www.w3.org/1998/Math/MathML'>
   <mover>
    <mi>v</mi>
     <mo>&#8594;</mo>
   </mover>
  </math>
 dans le rep�re
  <math xmlns='http://www.w3.org/1998/Math/MathML'>
   <mrow>
    <mo>(</mo>
    <mrow>
     <mi fontstyle='normal'>O</mi>
     <mo>,</mo>
     <mover>
      <mi>i</mi>
      <mo>&#8594;</mo>
     </mover>
     <mo>,</mo>
     <mover>
      <mi>j</mi>
      <mo>&#8594;</mo>
     </mover>
    </mrow>
    <mo>)</mo>
   </mrow>
  </math>.

</div>
<div class="wims_thm">
<h4>Propri�t�s</h4>
<ul>
<li>Si \(A\) et \(B\) sont deux points de coordonn�es respectives
  <math xmlns='http://www.w3.org/1998/Math/MathML'>
   <mrow>
    <mo>(</mo>
    <mrow>
     <msub>
      <mi>x</mi>
      <mi fontstyle='normal'>A</mi>
     </msub>
     <mo>;</mo>
     <msub>
      <mi>y</mi>
      <mi fontstyle='normal'>A</mi>
     </msub>
    </mrow>
    <mo>)</mo>
   </mrow>
  </math> et
  <math xmlns='http://www.w3.org/1998/Math/MathML'>
   <mrow>
    <mo>(</mo>
    <mrow>
     <msub>
      <mi>x</mi>
      <mi fontstyle='normal'>B</mi>
     </msub>
     <mo>;</mo>
     <msub>
      <mi>y</mi>
      <mi fontstyle='normal'>B</mi>
     </msub>
    </mrow>
    <mo>)</mo>
   </mrow>
  </math>
, alors le vecteur
  <math xmlns='http://www.w3.org/1998/Math/MathML'>
   <mover>
    <mi>AB</mi>
     <mo>&#8594;</mo>
   </mover>
  </math>
a pour coordonn�es
    <math xmlns='http://www.w3.org/1998/Math/MathML'>
   <mrow>
    <mo>(</mo>
    <mrow>
     <mrow>
      <msub>
       <mi>x</mi>
       <mi fontstyle='normal'>B</mi>
      </msub>
      <mo>-</mo>
      <msub>
       <mi>x</mi>
       <mi fontstyle='normal'>A</mi>
      </msub>
     </mrow>
     <mo>;</mo>
     <mrow>
      <msub>
       <mi>y</mi>
       <mi fontstyle='normal'>B</mi>
      </msub>
      <mo>-</mo>
      <msub>
       <mi>y</mi>
       <mi fontstyle='normal'>A</mi>
      </msub>
     </mrow>
    </mrow>
    <mo>)</mo>
   </mrow>
  </math>.</li>
<li>Si
  <math xmlns='http://www.w3.org/1998/Math/MathML'>
   <mover>
    <mi>u</mi>
     <mo>&#8594;</mo>
   </mover>
  </math> et <math xmlns='http://www.w3.org/1998/Math/MathML'>
   <mover>
    <mi>v</mi>
     <mo>&#8594;</mo>
   </mover>
  </math>
sont deux vecteurs de coordonn�es respectives
  <math xmlns='http://www.w3.org/1998/Math/MathML'>
   <mrow>
    <mo>(</mo>
    <mrow>
     <mi>x</mi>
     <mo>;</mo>
     <mi>y</mi>
    </mrow>
    <mo>)</mo>
   </mrow>
  </math>
 et
  <math xmlns='http://www.w3.org/1998/Math/MathML'>
   <mrow>
    <mo>(</mo>
    <mrow>
     <mrow>
      <mi>x</mi>
      <mo>&apos;</mo>
     </mrow>
     <mo>;</mo>
     <mrow>
      <mi>y</mi>
      <mo>&apos;</mo>
     </mrow>
    </mrow>
    <mo>)</mo>
   </mrow>
  </math>
, alors le vecteur
  <math xmlns='http://www.w3.org/1998/Math/MathML'>
   <mrow>
    <mover>
     <mi>u</mi>
      <mo>&#8594;</mo>
    </mover>
    <mo>+</mo>
    <mover>
     <mi>v</mi>
      <mo>&#8594;</mo>
    </mover>
   </mrow>
  </math>
a pour coordonn�es
  <math xmlns='http://www.w3.org/1998/Math/MathML'>
   <mrow>
    <mo>(</mo>
    <mrow>
     <mrow>
      <mi>x</mi>
      <mo>+</mo>
      <mrow>
       <mi>x</mi>
       <mo>&apos;</mo>
      </mrow>
     </mrow>
     <mo>;</mo>
     <mrow>
      <mi>y</mi>
      <mo>+</mo>
      <mrow>
       <mi>y</mi>
       <mo>&apos;</mo>
      </mrow>
     </mrow>
    </mrow>
    <mo>)</mo>
   </mrow>
  </math>.</li>
<li>Si
  <math xmlns='http://www.w3.org/1998/Math/MathML'>
   <mover>
    <mi>v</mi>
     <mo>&#8594;</mo>
   </mover>
  </math>
est un vecteur de coordonn�es
  <math xmlns='http://www.w3.org/1998/Math/MathML'>
   <mrow>
    <mo>(</mo>
    <mrow>
      <mi>x</mi>
     <mo>;</mo>
      <mi>y</mi>
    </mrow>
    <mo>)</mo>
   </mrow>
  </math>
et \(k\) un nombre r�el, alors le vecteur
  <math xmlns='http://www.w3.org/1998/Math/MathML'>
   <mrow>
    <mi>k</mi>
    <mo>&#8290;</mo>
    <mover>
     <mi>v</mi>
      <mo>&#8594;</mo>
    </mover>
   </mrow>
  </math>
a pour coordonn�es
  <math xmlns='http://www.w3.org/1998/Math/MathML'>
   <mrow>
    <mo>(</mo>
    <mrow>
     <mrow>
      <mi>k</mi>
      <mo>&#8290;</mo>
      <mi>x</mi>
     </mrow>
     <mo>;</mo>
     <mrow>
      <mi>k</mi>
      <mo>&#8290;</mo>
      <mi>y</mi>
     </mrow>
    </mrow>
    <mo>)</mo>
   </mrow>
  </math>.</li>
</ul>
</div>
