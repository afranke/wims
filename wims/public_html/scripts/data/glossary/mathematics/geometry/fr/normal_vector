!!abstract,linked gloses,internal links,content,dynamic examples,...
!set gl_author=Euler, Acad�mie de Versailles
!set gl_keywords=
!set gl_title=Vecteur normal � une droite du plan
:
:
:
:
<div class="wims_defn"><h4>D�finition</h4>
Soit D une droite du plan.<br/>
On appelle <strong>vecteur normal</strong> � D tout vecteur directeur d'une droite perpendiculaire � D.</div>
<div class="wims_thm"><h4>Th�or�me 1</h4>
Soit A un point et <math xmlns='http://www.w3.org/1998/Math/MathML'>
  <mover>
   <mi>v</mi>
    <mo>&#8594;</mo>
  </mover></math> un vecteur non nul du plan.<br/>
  La droite D passant par A et de vecteur normal <math xmlns='http://www.w3.org/1998/Math/MathML'>
  <mover>
   <mi>v</mi>
    <mo>&#8594;</mo>
  </mover></math> est l'ensemble des points M du plan tels que les vecteurs
  <math xmlns='http://www.w3.org/1998/Math/MathML'><mover>
   <mi>AM</mi>
    <mo>&#8594;</mo>
  </mover>
</math> et <math xmlns='http://www.w3.org/1998/Math/MathML'>
  <mover>
   <mi>v</mi>
    <mo>&#8594;</mo>
  </mover></math> soient orthogonaux.</div>
  <div class="wims_thm"><h4>Th�or�me 2</h4>
Deux droites du plan de vecteurs normaux respectifs <math xmlns='http://www.w3.org/1998/Math/MathML'>
  <mover>
   <mi>u</mi>
    <mo>&#8594;</mo>
  </mover></math> et <math xmlns='http://www.w3.org/1998/Math/MathML'>
  <mover>
   <mi>v</mi>
    <mo>&#8594;</mo>
  </mover></math> sont parall�les si et seulement si les vecteurs <math xmlns='http://www.w3.org/1998/Math/MathML'>
  <mover>
   <mi>u</mi>
    <mo>&#8594;</mo>
  </mover></math> et <math xmlns='http://www.w3.org/1998/Math/MathML'>
  <mover>
   <mi>v</mi>
    <mo>&#8594;</mo>
  </mover></math> sont colin�aires.
</div>
<div  class="wims_thm"><h4>Th�or�me 3</h4>
Le plan est muni d'un rep�re orthonormal.<br/>
 Si <math xmlns='http://www.w3.org/1998/Math/MathML'>
 <mrow>
  <mrow>
   <mrow>
    <mi>a</mi>
    <mo>&#8290;</mo>
    <mi>x</mi>
   </mrow>
   <mtext> </mtext>
   <mo>+</mo>
   <mtext> </mtext>
   <mrow>
    <mi>b</mi>
    <mo>&#8290;</mo>
    <mi>y</mi>
   </mrow>
   <mo>+</mo>
   <mi>c</mi>
  </mrow>
  <mo>=</mo>
  <mn>0</mn>
 </mrow>
</math> est une �quation cart�sienne de la droite D, alors le vecteur de coordonn�es <math xmlns='http://www.w3.org/1998/Math/MathML'>
 <mrow>
  <mo>(</mo>
  <mrow>
    <mi>a</mi>
   <mo>,</mo>
   <mi>b</mi>
  </mrow>
  <mo>)</mo>
 </mrow>
</math> est un vecteur normal � la droite D.
</div>
<div  class="wims_thm"><h4>Th�or�me 4</h4>
Le plan est muni d'un rep�re orthonormal.<br/>
Si D est une droite dont un vecteur normal a pour coordonn�es <math xmlns='http://www.w3.org/1998/Math/MathML'>
 <mrow>
  <mo>(</mo>
  <mrow>
    <mi>a</mi>
   <mo>,</mo>
   <mi>b</mi>
  </mrow>
  <mo>)</mo>
 </mrow>
</math>, alors il existe un r�el \(c\) tel que <math xmlns='http://www.w3.org/1998/Math/MathML'>
 <mrow>
  <mrow>
   <mrow>
    <mi>a</mi>
    <mo>&#8290;</mo>
    <mi>x</mi>
   </mrow>
   <mtext> </mtext>
   <mo>+</mo>
   <mtext> </mtext>
   <mrow>
    <mi>b</mi>
    <mo>&#8290;</mo>
    <mi>y</mi>
   </mrow>
   <mo>+</mo>
   <mi>c</mi>
  </mrow>
  <mo>=</mo>
  <mn>0</mn>
 </mrow>
</math> soit une �quation cart�sienne de D.
</div>
