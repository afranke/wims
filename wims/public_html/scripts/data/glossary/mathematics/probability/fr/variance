!!abstract,linked gloses,internal links,content,dynamic examples,...
!set gl_author=Sophie, Lemaire
!set gl_keywords=discrete_probability_distribution
!set gl_title=Variance d'une loi de probabilit�
!set gl_level=H5
:
:
:
:
<div class="wims_defn"><h4>D�finitions</h4>
Soit <math xmlns='http://www.w3.org/1998/Math/MathML'>
 <mi fontstyle="normal">&#937;</mi>
</math> l'univers associ� � une exp�rience al�atoire.<br/>
On suppose <math xmlns='http://www.w3.org/1998/Math/MathML'>
<mi fontstyle="normal">&#937;</mi>
</math> fini ; on note \(n\) le nombre d'�l�ments de
<math xmlns='http://www.w3.org/1998/Math/MathML'>
<mi fontstyle="normal">&#937;</mi>
</math> (\(n\) entier naturel non nul).<br/>
On suppose de plus que les \(n\) issues
<math xmlns='http://www.w3.org/1998/Math/MathML'>
 <mrow>
  <msub>
   <mi>x</mi>
   <mn>1</mn>
  </msub>
 </mrow></math>, <math xmlns='http://www.w3.org/1998/Math/MathML'>
 <mrow>
  <msub>
   <mi>x</mi>
   <mn>2</mn>
  </msub>
 </mrow></math>, &#8230; , <math xmlns='http://www.w3.org/1998/Math/MathML'>
 <mrow>
  <msub>
   <mi>x</mi>
   <mi>n</mi>
  </msub>
 </mrow>
</math> sont des nombres r�els et qu'une loi de probabilit� est d�finie sur
<math xmlns='http://www.w3.org/1998/Math/MathML'><mi fontstyle="normal">&#937;</mi>
</math> ; pour tout entier naturel \(i\) compris entre 1 et \(n\),
on note  <math xmlns='http://www.w3.org/1998/Math/MathML'><mrow>
  <msub>
   <mi>p</mi>
   <mi>i</mi>
  </msub>
 </mrow>
</math> la probabilit� de l'�v�nement �l�mentaire  <math xmlns='http://www.w3.org/1998/Math/MathML'>
 <mrow>
  <mrow>
   <mo>{</mo>
   <msub>
    <mi>x</mi>
    <mi>i</mi>
   </msub>
   <mo>}</mo>
  </mrow>
 </mrow>
</math> et <math xmlns='http://www.w3.org/1998/Math/MathML'>
<mi fontstyle="normal">&#956;</mi>
</math> l'esp�rance de la loi de probabilit�.
<br/>
La <strong>variance</strong> de la loi de probabilit� est le nombre \(V\) d�fini par :
<math xmlns='http://www.w3.org/1998/Math/MathML'>
 <mrow>
  <mi>V</mi>
  <mo>=</mo>
  <mrow>
   <mo>&#8290;</mo>
   <munderover>
    <mrow>
     <mtext> </mtext>
     <mstyle scriptlevel='0'>
      <mo>&#8721;</mo>
     </mstyle>
    </mrow>
    <mrow>
     <mi>i</mi>
     <mo>=</mo>
     <mn>1</mn>
    </mrow>
    <mi>n</mi>
   </munderover>
   <mo>&#8290;</mo>
   <msup>
    <mrow>
     <msub>
      <mi>p</mi>
      <mi>i</mi>
     </msub>
     <mo>(</mo>
     <mrow>
      <msub>
       <mi>x</mi>
       <mi>i</mi>
      </msub>
      <mo>-</mo>
 <mi fontstyle="normal">&#956;</mi>
     </mrow>
     <mo>)</mo>
    </mrow>
    <mn>2</mn>
   </msup>
  </mrow>
 </mrow>
</math>.
</div>
<div class="wims_rem">
La variance d'une loi de probabilit� est un r�el positif.</div>
<div class="wims_thm"><h4>Propri�t�</h4>
La variance \(V\) peut aussi �tre calcul�e � l'aide de la formule : <math xmlns='http://www.w3.org/1998/Math/MathML'>
 <mrow>
  <mi>V</mi>
  <mo>=</mo>
  <mrow>
   <mrow>
    <mo>(</mo>
    <mrow>
     <munderover>
    <mrow>
     <mtext> </mtext>
     <mstyle scriptlevel='0'>
      <mo>&#8721;</mo>
     </mstyle>
    </mrow>
    <mrow>
     <mi>i</mi>
     <mo>=</mo>
     <mn>1</mn>
    </mrow>
    <mi>n</mi>
   </munderover>
     <mtext> </mtext>
     <mrow>
      <msub>
       <mi>p</mi>
       <mi>i</mi>
      </msub>
      <mo>&#8290;</mo>
      <msup>
       <msub>
        <mi>x</mi>
        <mi>i</mi>
       </msub>
       <mn>2</mn>
      </msup>
     </mrow>
    </mrow>
    <mo>)</mo>
   </mrow>
   <mo>-</mo>
   <msup>
<mi fontstyle="normal">&#956;</mi>
    <mn>2</mn>
   </msup>
  </mrow>
 </mrow>
</math>.
</div>



