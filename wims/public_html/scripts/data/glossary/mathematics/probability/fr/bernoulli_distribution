!!abstract,linked gloses,internal links,content,dynamic examples,...
!set gl_author=Euler, Acad�mie de Versailles
!set gl_keywords=discrete_probability_distribution
!set gl_title=Loi de Bernoulli
!set gl_level=H5
:
:
:
:
<div class="wims_defn">
  <h4>
    D�finition
  </h4>
  Soit \(p\) un nombre r�el appartenant �
  <math xmlns='http://www.w3.org/1998/Math/MathML'>
   <mrow>
    <mo>[</mo>
    <mrow>
     <mn>0</mn>
     <mo>;</mo>
     <mn>1</mn>
    </mrow>
    <mo>]</mo>
   </mrow>
  </math>.
  <br/>
  On appelle <strong>�preuve de Bernoulli de param�tre \(p\)</strong> toute exp�rience al�atoire n'admettant que deux issues
  <math xmlns='http://www.w3.org/1998/Math/MathML'>
    <mi fontstyle='normal'>A</mi>
  </math>
  et
  <math xmlns='http://www.w3.org/1998/Math/MathML'>
   <mover>
    <mi fontstyle='normal'>A</mi>
    <mo>_</mo>
   </mover>
  </math>
  de probabilit�s respectives \(p\) et
  <math xmlns='http://www.w3.org/1998/Math/MathML'>
   <mrow>
    <mi>q</mi>
    <mo>=</mo>
    <mrow>
     <mn>1</mn>
     <mo>-</mo>
     <mi>p</mi>
    </mrow>
   </mrow>
  </math>.
</div>
<div class="wims_defn">
  <h4>
    D�finition
  </h4>
  Soit
  <math xmlns='http://www.w3.org/1998/Math/MathML'>
   <mrow>
    <mi fontstyle='normal'>&#937;</mi>
    <mo>=</mo>
    <mrow>
     <mo>{</mo>
     <mrow>
      <mn>0</mn>
      <mo>;</mo>
      <mn>1</mn>
     </mrow>
     <mo>}</mo>
    </mrow>
    </mrow>
  </math>
  l'univers associ� � une exp�rience al�atoire, et soit \(p\) un nombre r�el appartenant �
  <math xmlns='http://www.w3.org/1998/Math/MathML'>
   <mrow>
    <mo>[</mo>
    <mrow>
     <mn>0</mn>
     <mo>;</mo>
     <mn>1</mn>
    </mrow>
    <mo>]</mo>
   </mrow>
  </math>.
  <br/>
  On appelle <strong>loi de Bernoulli de param�tre \(p\)</strong> la loi de probabilit� d�finie sur
  <math xmlns='http://www.w3.org/1998/Math/MathML'>
   <mi fontstyle='normal'>&#937;</mi>
  </math>
  par
<math xmlns='http://www.w3.org/1998/Math/MathML'>
 <mrow>
  <mo>{</mo>
  <mtable columnalign='left'>
   <mtr>
    <mtd>
     <mrow>
      <mrow>
       <mi>p</mi>
       <mo>&#8289;</mo>
       <mo>(</mo>
       <mrow>
        <mo>{</mo>
        <mn>1</mn>
        <mo>}</mo>
       </mrow>
       <mo>)</mo>
      </mrow>
      <mo>=</mo>
      <mi>p</mi>
     </mrow>
    </mtd>
   </mtr>
   <mtr>
    <mtd>
     <mrow>
      <mrow>
       <mi>p</mi>
       <mo>&#8289;</mo>
       <mo>(</mo>
       <mrow>
        <mo>{</mo>
        <mn>0</mn>
        <mo>}</mo>
       </mrow>
       <mo>)</mo>
      </mrow>
      <mo>=</mo>
      <mrow>
       <mn>1</mn>
       <mo>-</mo>
       <mi>p</mi>
      </mrow>
     </mrow>
    </mtd>
   </mtr>
  </mtable>
 </mrow>
</math>.
</div>

<div class="wims_thm">
  <h4>
    Th�or�me
  </h4>
  Soit
  <math xmlns='http://www.w3.org/1998/Math/MathML'>
   <mrow>
    <mi>p</mi>
    <mo>&#8712;</mo>
    <mrow>
     <mo>[</mo>
     <mrow>
      <mn>0</mn>
      <mo>;</mo>
      <mn>1</mn>
     </mrow>
     <mo>]</mo>
    </mrow>
   </mrow>
  </math>
  et
  <math xmlns='http://www.w3.org/1998/Math/MathML'>
   <msub>
    <mi>B</mi>
    <mi>p</mi>
   </msub>
  </math>
  la loi de Bernoulli de param�tre \(p\).
  <br/>
  On pose
  <math xmlns='http://www.w3.org/1998/Math/MathML'>
   <mrow>
    <mi>q</mi>
    <mo>=</mo>
    <mrow>
     <mn>1</mn>
     <mo>-</mo>
     <mi>p</mi>
    </mrow>
   </mrow>
  </math>.
  <ul>
    <li>
      L'esp�rance math�matique E de
      <math xmlns='http://www.w3.org/1998/Math/MathML'>
       <msub>
        <mi>B</mi>
        <mi>p</mi>
       </msub>
      </math>
      est
      <math xmlns='http://www.w3.org/1998/Math/MathML'>
       <mrow>
        <mi fontstyle='normal'>E</mi>
        <mo>=</mo>
        <mi>p</mi>
       </mrow>
      </math> ;
    </li>
    <li>
      La variance V de
      <math xmlns='http://www.w3.org/1998/Math/MathML'>
       <msub>
        <mi>B</mi>
        <mi>p</mi>
       </msub>
      </math>
      est
      <math xmlns='http://www.w3.org/1998/Math/MathML'>
       <mrow>
        <mi fontstyle='normal'>V</mi>
        <mo>=</mo>
        <mrow>
         <mi>p</mi>
         <mo>&#8290;</mo>
         <mi>q</mi>
        </mrow>
       </mrow>
      </math>.
    </li>
  </ul>
</div>
