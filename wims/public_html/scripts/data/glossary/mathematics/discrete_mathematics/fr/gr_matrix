!!abstract,linked gloses,internal links,content,dynamic examples,...
!set gl_author=Euler, Acad�mie de Versailles
!set gl_keywords=graph
!set gl_title=Matrice associ�e � un graphe
!set gl_level=H6 ES
:
:
:
:
<div class="wims_defn">
  <h4>D�finition</h4>
  Soit
  \(n\in\NN^*\)
  et \(G\) un graphe non orient� d'ordre \(n\).
  <br/>
  On appelle <strong>matrice associ�e</strong> � \(G\) la matrice carr�e
  \(A=(a_{i,j}) \)
  de dimension \(n\) telle que :
  <ul>
    <li>
      \(a_{i,j}=1\)
      s'il existe une ar�te d'extr�mit�s \(i\) et \(j\) ;
    </li>
    <li>
      \(a_{i,j}=0\) sinon.
    </li>
  </ul>
</div>
<div class="wims_rem">
  <h4>Remarque</h4>
  La matrice associ�e � un graphe non orient� est sym�trique, c'est-�-dire que,
  pour tous entiers \(i\) et \(j\) tels que
  \(1 \leqslant i \leqslant n\)
  et
  \(1 \leqslant j \leqslant n\),
  on a :
  \(a_{j,i}=a_{i,j}\).
</div>
<div class="wims_defn">
  <h4>
    D�finition
  </h4>
  Soit \(\n \in \NN^*\) et \(G\) un graphe orient� d'ordre \(n\).
  <br/>
  On appelle <strong>matrice associ�e</strong> � \(G\) la matrice carr�e
  \(A=(a_{i,j})\) de dimension \(n\) telle que :
  <ul>
    <li>
      \(a_{i,j}=1\) s'il existe une ar�te d'origine \(i\) et d'extr�mit� \(j\) ;
    </li>
    <li>
      \(a_{i,j}=0\) sinon.
    </li>
  </ul>
</div>
:mathematics/discrete_mathematics/fr/graphdraw_1
