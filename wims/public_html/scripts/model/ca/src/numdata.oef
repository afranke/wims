type=gapfill
textarea="data"

:Preguntes de resposta num�rica incrustades en un text aleatori.

Aquest exercici presenta textos "amb forats", cadascun per a ser omplert per un
nombre.
<p>
Per construir un exercici amb aquest model, nom�s cal que introdu�u els textos.
Una senzilla sintaxi us permet especificar els forats i el nombre de forats
que accepta.
<P>
Autor del model: Gang Xiao <qualite@wimsedu.info>

:%%%%%%%%%%%%%%%%%      ATTENTION      %%%%%%%%%%%%%%%%%%%%

Enlevez l'en-t�te ci-dessus si vous d�truisez les balises pour le mod�le !
(Ce sont les lignes qui commencent par un ':'.)
Sinon l'exercice risque de ne pas pouvoir repasser sous Createxo.

:%%%%%%%% Param�tres d'exemples � red�finir %%%%%%%%%%%%%%%%%

:\title{Preguntes num�riques}
:\author{XIAO, Gang}
:\email{qualite@wimsedu.info}
:\credits{}

:S'accepta text aleatori posat entre claus
$embraced_randitem
\text{accolade=item(1,1 s�,
2 no)}

:Dades principals.
Definiu una pregunta per l�nia i l�nies separades
per un punt i coma. (Els textos, per tant, no poden contenir
punt i coma). <p>
Les dades num�riques que cal respondre s'han de posar entre un parell de
doble-signe d'interrogaci� "??".
Podeu fer fins a 6 preguntes en una sola frase.
$embraced_randitem
\text{data=
Le permis � points contient ??6?? points pour un nouveau conducteur.;
Le permis � points aura ??12?? points au bout de ??3?? ans si le
	nouveau conducteur ne s'est pas fait retirer de point pendant
	cette p�riode et n'a pas suivi d'apprentissage anticip�.;
Le permis � points aura ??12?? points au bout de ??2?? ans si le
	nouveau conducteur ne s'est pas fait retirer de point pendant
	cette p�riode et a suivi un apprentissage anticip�.;
Le permis � points "plein" contient ??12?? points.;
L'oubli de la ceinture de s�curit� co�te ??1?? point(s) sur le permis �
	points.;
Le chevauchement de la ligne continue au milieu de la route co�te
	??1?? point(s) sur le permis � points.;
Si je ne boucle pas ma ceinture, �a me co�tera ??1?? point(s) sur mon permis.;
Si j'oublie de porter un casque quand je conduis une moto, �a me co�tera ??1?? point(s)
	sur mon permis.;
Si j'acc�l�re alors que quelqu'un est en train de me d�passer, �a me co�tera
	??2?? point(s) sur mon permis.;
{La circulation,Le stationnement} sur le terre-plein central d'une autoroute
	co�te ??2?? point(s) sur le permis.;
Le franchissement de la ligne continue au milieu de la route co�te
	??3?? point(s) sur le permis � points.;
La conduite avec un taux d'alcool�mie dans le sang compris entre 0.5 g/l et
	moins de 0.8 g/l co�te ??3?? point(s) sur le permis � points.;
La conduite avec un taux d'alcool�mie dans le sang compris entre ??0.5?? g/l et
	moins de ??0.8?? g/l co�te 3 points sur le permis � points.;
Le d�passement dangereux co�te ??3?? points sur le permis.;
{L'arr�t,Le stationnement} dangereux co�te ??3?? point(s) sur le permis.;
Si je {roule,circule} sur la bande d'arr�t d'urgence d'une autoroute, �a me co�tera
	??3?? points sur mon permis.;
Le non-respect de la distance de s�curit� co�te ??3?? point(s) sur le permis.;
Un exc�s de vitesse de {5,8,10,15,20,25} km/h me co�tera ??3?? point(s)
	pendant la p�riode de probation de 2 ans.;
Le non-respect d'un stop co�te ??4?? point(s) sur le permis.;
Si je grille un feu rouge, �a me co�tera ??4?? point(s) sur mon permis.;
La conduite avec un taux dans le sang d'alcool�mie de {0.8,0.9,1,1.2,1.3,1.5} g
	pour mille co�te ??6?? point(s) sur le permis � points.;
La conduite avec un taux dans le sang d'alcool�mie sup�rieur ou �gale �
	??0.8?? g pour mille co�te 6 point(s) sur le permis � points.;
La circulation en sens interdit co�te ??4?? point(s) sur le permis.;
Le non-respect de la priorit� de passage co�te ??4?? point(s) sur le permis.;
{La marche arri�re,Le demi-tour} sur l'autoroute co�te ??4?? point(s) sur le
	permis.;
Le d�lit de fuite co�te ??6?? point(s) sur le permis.;
}

:%%%%%%%%%%%%%% Rien � modifier avant l'�nonc� %%%%%%%%%%%%%%%%
\text{accolade=wims(word 1 of \accolade)}

\text{data=wims(singlespace \data)}
\text{data=wims(nonempty rows \data)}
\text{data=randomrow(\data)}
\text{data=\accolade=1 ? wims(embraced randitem \data)}
\text{data=slib(text/cutchoice2 \data)}
\integer{qs=floor(rows(\data)/2)}
\text{len=}
\for{i=1 to \qs}{
 \text{d=wims(trim \data[2*\i;])}
 \integer{l=wims(charcnt \d) + 2}
 \text{len=\len \l,}
}

\text{qlist=wims(makelist reply x for x=1 to \qs)}
\steps{\qlist}

:%%%%%%%%%%%%% Maintenant l'�nonc� en code html. %%%%%%%%%%%%%%%%%%%%

::Vous n'avez pas besoin de modifier ceci en g�n�ral.

\statement{
\data[1;]
\for{k=1 to \qs}{
\embed{r \k,\len[\k]
autocomplete="off"} \data[2*\k+1;]
}
}

:%%%%%%%%%%%%% Rien � modifier ci-apr�s. %%%%%%%%%%%%%%%%%5

\answer{Champ 1}{\data[2;]}{type=number}
\answer{Champ 2}{\data[4;]}{type=number}
\answer{Champ 3}{\data[6;]}{type=number}
\answer{Champ 4}{\data[8;]}{type=number}
\answer{Champ 5}{\data[10;]}{type=number}
\answer{Champ 6}{\data[12;]}{type=number}

