type=question first
textarea="instruction datatrue datafalse explain"
asis="datatrue datafalse explain"

:Una pregunta d'opcions m�ltiples amb permutaci� aleat�ria d'opcions.

Aquest �s un model de pregunta d'opcions m�ltiples. L'ordre de les opcions �s aleatori, 
aix� com les opcions si n'hi ha prou de definides.
Quan hi ha diverses opcions correctes, nom�s cal triar-ne una, no importa quina.

<p>El text no �s aleatori. Un altre model <b>Opci� m�ltiple avan�ada QCM</b>
permet tenir tamb� un text aleatori.</p>


$image_help

$math_help

<p>Autor del model: Gang Xiao <qualite@wimsedu.info></p>

:%%%%%%%%%%%%%%%%%      ATENCI�      %%%%%%%%%%%%%%%%%%%%

Elimineu la cap�alera anterior si esborreu les etiquetes per al model!
(S�n les l�nies que comencen amb un ':'.)
En cas contrari l'exercici pot no funcionar sota Createxo.

:%%%%%%%% Par�metres d'exemple a redefinir %%%%%%%%%%%%%%%%%

:\title{Opcions m�ltiples}
:\author{XIAO, Gang}
:\email{qualite@wimsedu.info}
:\credits{}

:Nombre d'opcions a presentar. Com a m�xim 10
Has de definir prou opcions per escollir. En cas contrari l'exercici nom�s mostrar� el que est� disponible.
\integer{tot=5}

:El nombre d'opcions correctes en cada exercici. No pot excedir el total
\integer{givetrue=2}

:Nombre d'opcions incorrectes "obligat�ries"
Per exemple, si aquest n�mero �s 2, als exercicis generats sempre es mostraran les dues primeres opcions incorrectes de la llista.
<p> En cas de dubte, posar 0.</p>
\integer{minfalse=0}

:L'enunciat de l'exercici
$embraced_randitem
\text{explain=Aquest �s un model de pregunta d'opcions m�ltiples. L'ordre de les opcions �s aleatori, aix� com les opcions si n'hi ha prou de definides.
Canvieu els textos i tindreu la vostra pregunta d'opcions m�ltiples.<br/>
Trieu l'opci� correcta. <br/>
Quan hi ha diverses opcions correctes, nom�s cal triar-ne una, no importa quina.
}

:L'(es) opci�(ons) correcta(es), una per l�nia
Se'n poden col�locar diverses (una per l�nia) per triar a l'atzar.
Eviteu frases massa llargues. Cap punt i coma a la frase.

$embraced_randitem
\matrix{datatrue=Opci� correcta n�1
Opci� correcta n�2
Opci� correcta n�3
Opci� correcta n�4
}

:Les opcions incorrectes, una per l�nia
Se'n poden col�locar diverses (una per l�nia) per triar a l'atzar.
Eviteu frases massa llargues! Cap punt i coma a la frase.
$embraced_randitem
\matrix{datafalse=Opci� incorrecta n�1
Opci� incorrecta n�2
Opci� incorrecta n�3
Opci� incorrecta n�4
Opci� incorrecta n�5
Opci� incorrecta n�6
Opci� incorrecta n�7
Opci� incorrecta n�8
}

:Opcions:  <span class="tt wims_code_words">checkbox</span>,  <span class="tt wims_code_words">split</span>
Afegir la paraula <span class="tt wims_code_words">checkbox</span> si hi ha diverses opcions correctes i l'estudiant ha d'escollir totes les opcions correctes (en lloc de nom�s una).
En aquest cas, tamb� afegir la paraula <span class="tt wims_code_words">split</span> si permetem una nota parcial quan nom�s es tria una part de les respostes correctes.
\text{option=}

:Feedback general
Aquest text apareixer� despr�s de la resposta de l'alumne, estigui b� o malament.
\text{feedback_general=}

:Feedback en cas de resposta correcta
Aquest text apareixer� despr�s de la resposta de l'alumne, si la resposta �s bona o en cas de resposta parcial.
\text{feedback_bon=}

:Feedback en cas de resposta equivocada
Aquest text apareixer� despr�s de la resposta de l'alumne, si ha escollit com a m�nim una resposta incorrecta.
\text{feedback_mauvais=}

:Indicaci�
Un text que pot ajudar l'alumne a trobar la resposta correcta
\text{exo_hint=}

:S'accepta text aleatori posat entre claus
$embraced_randitem
\text{accolade=item(1,1 s�,
2 no)}


:%%%%%%%%%%%%%% Res a canviar abans de l'enunciat %%%%%%%%%%%%%%%%
\text{accolade=wims(word 1 of \accolade)}

\text{empty=}
\if{\feedback_general != \empty}{
  \text{feedback_general=<p class="feedback">\feedback_general</p>}
}
\if{\feedback_bon != \empty}{
  \text{feedback_bon=<p class="feedback good_answer">\feedback_bon</p>}
}
\if{\feedback_mauvais != \empty}{
  \text{feedback_mauvais=<p class="feedback bad_answer">\feedback_mauvais</p>}
}
\if{\exo_hint != \empty}{
  \hint{\exo_hint}
}

\text{datatrue=wims(nonempty rows \datatrue)}
\text{datafalse=wims(nonempty rows \datafalse)}
\integer{truecnt=rows(\datatrue)}
\integer{falsecnt=rows(\datafalse)}
\integer{givetrue=\givetrue<1?1}
\integer{givetrue=\givetrue>\truecnt?\truecnt}
\integer{tot=\tot > \falsecnt+\givetrue?\falsecnt+\givetrue}
\integer{givetrue=\givetrue>\tot-1?\tot-1}
\integer{minfalse=\minfalse>\tot-\givetrue?\tot-\givetrue}
\text{tsh=shuffle(\truecnt)}
\text{true=row(\tsh,\datatrue)}
\if{\minfalse>0}{
 \text{false1=row(1..\minfalse,\datafalse);}
 \text{false2=row(\minfalse+1..\falsecnt,\datafalse)}
}{
 \integer{minfalse=0}
 \text{false1=}
 \text{false2=\datafalse}
}
\text{fsh=shuffle(\falsecnt)}
\text{false2=row(\fsh,\false2)}
\text{pick=row(1..\givetrue,\true);\false1 row(1..\tot-\givetrue-\minfalse,\false2)}
\text{ind=wims(makelist 1 for x=1 to \givetrue),wims(makelist 0 for x=1 to \tot-\givetrue)}

\text{sh=shuffle(\tot)}
\text{ind=item(\sh,\ind)}
\text{pick=row(\sh,\pick)}
\text{pick=\accolade=1 ? wims(embraced randitem \pick)}
\text{explain=\accolade=1 ? wims(embraced randitem \explain)}
\text{ans=positionof(1,\ind)}
\text{list=item(1..\tot,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z)}
\text{anstype=checkbox iswordof \option?checkbox:radio}

:%%%%%%%%%%%%% Ara l'enunciat en format HTML. %%%%%%%%%%%%%%%%%%%%

::Vost� no necessita canviar aix� en general.

\statement{
  <div class="oef_explain">\explain</div>
  \for{i=1 to \tot}
  {<div class="oefreply">\embed{reply 1,\i, \list[\i]�: \pick[\i;]}</div>}
}

:%%%%%%%%%%%%% Res a canviar per sota. %%%%%%%%%%%%%%%%%5

\answer{Resposta}{\ans;\list}{type=\anstype}{option=\option}

\feedback{1=1}{\feedback_general}
\feedback{\reply1 isitemof \list[\ans]}{\feedback_bon}
\feedback{\reply1 notitemof \list[\ans]}{\feedback_mauvais}
